<?php
$input = (object) array();
$input->origin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : $_SERVER['SERVER_NAME'];
$input->url = parse_url($_SERVER['REQUEST_URI']);
$input->path = explode('/',substr($input->url['path'],1));
$input->server = substr($_SERVER['SERVER_NAME'],4);

header("Access-Control-Allow-Origin: " . $input->origin);
header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, OPTIONS");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Content-Type, Accept, Authorization, X-Requested-With");
header("Access-Control-Max-Age: 1");
header('Content-Type: application/json;charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == "OPTIONS")
	die(http_response_code(200));

foreach ($input->path as $key => $value)
	if (!preg_match("/^[a-z0-9_\-]+$/", $value))
		$result = array("code" => 400, "message" => "Chemin invalide");

if (!isset($result))
{
	if (@$_GET['data'] && @$_GET['data']!='{}' && ($_SERVER['REQUEST_METHOD']=='GET' OR $_SERVER['REQUEST_METHOD']=='DELETE'))
		$input->body = json_decode(urldecode($_GET['data']));
	else if (file_get_contents("php://input") && file_get_contents("php://input")!='{}')
		$input->body = json_decode(file_get_contents("php://input"));

 	if (json_last_error() !== JSON_ERROR_NONE)
		$result = array("code" => 400, "message" => "JSON invalide");
}

if (!isset($result))
	if ($input->path[2] == 'service')
		include_once 'resources/service.php';
	else if (isset($input->path[7]))
		if (file_exists('resources/' . $input->path[3] . '_' . $input->path[5] . '_' . $input->path[7] . '.php'))
			include_once 'resources/' . $input->path[3] . '_' . $input->path[5] . '_' . $input->path[7] .'.php';
		else
			$result = array("code" => 400, "message" => "Resource inconnue");
	else if (isset($input->path[5]))
		if (file_exists('resources/' . $input->path[3] . '_' . $input->path[5]  . '.php'))
			include_once 'resources/' . $input->path[3] . '_' . $input->path[5] . '.php';
		else
			$result = array("code" => 400, "message" => "Resource inconnue");
	else if (isset($input->path[3]))
		if (file_exists('resources/' . $input->path[3] . '.php'))
			include_once 'resources/' . $input->path[3] . '.php';
		else
			$result = array("code" => 400, "message" => "Resource inconnue");
	else
		$result = array("code" => 400, "message" => "Ressource inconnue");

if ($input->path[2] == 'resources')
	$result = array("code" => 200, "data"=>$resource);
	
$connection = new PDO("mysql:host=127.0.0.1", getenv('MARIADB_API_USER'), getenv('MARIADB_API_PASSWORD'));
	
include_once 'libs/JWT.php';
include_once 'libs/functions.php';

if (!isset($result))
	$result = function_exists(strtolower($_SERVER['REQUEST_METHOD'])) ? strtolower($_SERVER['REQUEST_METHOD'])() : array("code" => 501, "message" => 'Méthode non implémentée');

http_response_code($result['code']);
echo json_encode($result);
?>