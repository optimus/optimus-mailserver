FROM php:7.4-fpm

ENV DEBIAN_FRONTEND noninteractive
ENV TZ Europe/Paris

RUN apt-get update && apt-get install -y -qq \
	cron \
	sed \
	postfix \
	postfix-mysql \
	sasl2-bin \
	libsasl2-modules \
	libsasl2-modules-sql \
	dovecot-imapd \
	dovecot-mysql \
	dovecot-sieve \
	dovecot-managesieved \
	spamass-milter \
	clamav-milter \
	mailutils \
	opendkim \
	opendkim-tools \
	opendmarc \
	&& rm -rf /var/lib/apt/lists/* \
	&& docker-php-ext-install pdo_mysql

RUN mkdir -p /srv/init && mkdir -p /srv/www \
	&& touch /usr/local/etc/php-fpm.d/logging.conf

COPY ./optimus-container/php.ini /usr/local/etc/php/conf.d
COPY ./optimus-libs/init.php /srv/init
COPY ./optimus-container/www /srv/www
COPY ./optimus-container/sql /srv/sql

COPY ./optimus-mailserver/etc /etc
RUN chmod +646 -R /etc/default \
	&& chmod +646 -R /etc/postfix \
	&& chmod +646 -R /etc/dovecot \
	&& chmod +646 -R /etc/spamassassin \
	&& chmod +646 -R /etc/clamav \
	&& chmod +646 -R /etc/opendkim \
	&& chmod +646 -R /etc/opendmarc

RUN rm /etc/opendkim.conf && ln -s /etc/opendkim/opendkim.conf /etc/opendkim.conf
RUN rm /etc/opendmarc.conf && ln -s /etc/opendmarc/opendmarc.conf /etc/opendmarc.conf

COPY ./optimus-mailserver/init.sh /srv/init.sh

RUN chown www-data /usr/local/etc/php/conf.d/php.ini \
	&& chown www-data /usr/local/etc/php-fpm.d/zz-docker.conf \
	&& chown www-data /usr/local/etc/php-fpm.d/logging.conf

RUN groupadd -g 203 mailboxes && useradd -s "/usr/sbin/nologin" -g 203 -u 203 mailboxes
RUN mkdir -p /srv/mailboxes && chown mailboxes:mailboxes /srv/mailboxes && chmod 770 -R /srv/mailboxes && touch /var/log/dovecot-debug.log && touch /var/log/dovecot-info.log && touch /var/log/dovecot.log && chmod 644 /var/log/dovecot*

EXPOSE 25 143 465 587 993

CMD /bin/bash /srv/init.sh
# CMD /usr/sbin/syslogd && service postfix start && dovecot -F