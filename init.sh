#!/bin/bash

files=(
'/etc/clamav/clamav-milter.conf' 
'/etc/clamav/virusaction.sh' 
'/etc/default/clamav-milter' 
'/etc/default/saslauthd' 
'/etc/default/spamass-milter' 
'/etc/default/spamassassin' 
'/etc/dovecot/dovecot-dict-sql.conf' 
'/etc/dovecot/dovecot-sql.conf' 
'/etc/dovecot/dovecot.conf' 
'/etc/dovecot/quota_warning.sh' 
'/etc/postfix/sasl/smtpd.conf' 
'/etc/postfix/aliases.cf' 
'/etc/postfix/maildirs.cf' 
'/etc/postfix/main.cf' 
'/etc/postfix/master.cf' 
'/etc/postfix/recipient_bcc.cf' 
'/etc/postfix/redirections.cf' 
'/etc/postfix/sender_bcc.cf' 
'/etc/postfix/smtpauth.cf' 
'/etc/postfix/transport.cf' 
'/etc/postfix/unsubscribe.cf' 
'/etc/postfix/virtual_domains.cf' 
'/etc/spamassassin/local.cf' 
'/etc/opendkim/opendkim.conf'
'/etc/opendmarc/opendmarc.conf' 
)

for file in ${files[@]}; do
    sed -i 's/$AES_KEY/'$AES_KEY'/g' $file
    sed -i 's/$DOMAIN/'$DOMAIN'/g' $file
    sed -i 's/$MAILSERVER_MARIADB_USER/'$NAME'/g' $file
    HEXKEY=$(echo $AES_KEY | od -A n -t x1 | sed 's/ *//g' | tr -d '\n')
    HEXKEY=${HEXKEY:0:-2}
    MAILSERVER_MARIADB_PASSWORD=$(echo -n "password_for_$NAME" | openssl enc -aes-128-ecb -base64 -K "$HEXKEY")
    sed -i 's~$MAILSERVER_MARIADB_PASSWORD~'$MAILSERVER_MARIADB_PASSWORD'~g' $file
    chmod 644 $file
done 

php /srv/init/init.php

#   envsubst '${DOMAIN}' < /etc/allspark/mailserver/dovecot/default.sieve > /srv/mailboxes/default.sieve




#   [ $(getent group mailboxes) ] || verbose groupadd mailboxes --gid 203
#   [ $(getent passwd mailboxes) ] || verbose useradd -g mailboxes -s /bin/false -d /srv/mailboxes --uid 203 mailboxes
#   verbose usermod -a -G mailboxes www-data

#   echo_magenta "Création des dossiers"
#   verbose mkdir -p /srv/mailboxes
#   verbose chown mailboxes:mailboxes /srv/mailboxes
#   verbose chmod 770 -R /srv/mailboxes
  
#   echo_magenta "Création du vhost mail.$DOMAIN..."
#   if [ ! -d "/srv/www" ]; then verbose mkdir /srv/www; fi
#   verbose touch /srv/www/index.php
#   if [ ! -f "/etc/apache2/sites-enabled/mail.conf" ]; then sed -e 's/%DOMAIN%/'$DOMAIN'/g' /etc/allspark/mailserver/vhost > /etc/apache2/sites-enabled/mail.conf; fi
#   chown -R www-data:www-data /srv/www

#   echo_magenta "Création de l'utilisateur MARIADB"
#   verbose mariadb -u root -e "GRANT SELECT, INSERT, UPDATE, DELETE ON mailserver.* TO '$MAILSERVER_MARIADB_USER'@'127.0.0.1' IDENTIFIED BY '$MAILSERVER_MARIADB_PASSWORD';"
#   verbose mariadb -u root -e "GRANT SELECT ON server.users TO '$MAILSERVER_MARIADB_USER'@'127.0.0.1' IDENTIFIED BY '$MAILSERVER_MARIADB_PASSWORD';"
#   verbose mariadb -u root -e "FLUSH PRIVILEGES"

#   echo_magenta "Installation des bases de données MARIADB"
#   if [ -f "/srv/databases/MAILSERVER_DB_VERSION" ]; then db_version=$(cat /srv/databases/MAILSERVER_DB_VERSION); fi

#   for file in /etc/allspark/mailserver/*.sql
#   do
#     file="${file:25:-4}"
#     if [[ $file > $db_version ]]
#     then
#       echo_magenta "--> $file.sql exécuté"
#       mariadb < /etc/allspark/mailserver/$file.sql
#       update_conf MAILSERVER_DB_VERSION $file
#     else
#       echo_magenta "--> $file.sql ignoré"
#     fi
#   done

#   echo_magenta "Attribution des droits d'accès à l'API ALLSPARK"
#   verbose mariadb -u root -e "GRANT SELECT, INSERT, UPDATE, DELETE ON mailserver.mailboxes TO '$ALLSPARK_API_MARIADB_USER'@'localhost' IDENTIFIED BY '$ALLSPARK_API_MARIADB_PASSWORD';"
#   verbose mariadb -u root -e "GRANT SELECT, INSERT, UPDATE, DELETE ON mailserver.mailboxes_domains TO '$ALLSPARK_API_MARIADB_USER'@'localhost' IDENTIFIED BY '$ALLSPARK_API_MARIADB_PASSWORD';"

#   echo_magenta "Création de la boite mail initiale $ALLSPARK_ADMIN_USER@$DOMAIN"
#   verbose mariadb -u root -e "INSERT IGNORE INTO mailserver.mailboxes_domains VALUES (NULL, 1, '$DOMAIN');"
#   verbose mariadb -u root -e "INSERT IGNORE INTO mailserver.mailboxes VALUES (NULL, '$ALLSPARK_ADMIN_USER@$DOMAIN', 0, 1, 'postmaster@$DOMAIN;root@$DOMAIN;fail2ban@$DOMAIN', null, null, null, null);"
#   verbose mkdir -p "/srv/mailboxes/$ALLSPARK_ADMIN_USER@$DOMAIN"
#   verbose chown mailboxes:mailboxes "/srv/mailboxes/$ALLSPARK_ADMIN_USER@$DOMAIN"
#   verbose chmod 770 -R "/srv/mailboxes/$ALLSPARK_ADMIN_USER@$DOMAIN"


#   echo_magenta "Ouverture des ports du Firewall"
#   if [ $(which /sbin/ufw) ]
#   then
#     verbose /sbin/ufw allow 993
#     verbose /sbin/ufw allow 587
#     verbose /sbin/ufw allow 465
#     verbose /sbin/ufw allow 143
#     verbose /sbin/ufw allow 25
#   fi

#   echo_magenta "Installation des paquets de POSTFIX"
#   DEBIAN_FRONTEND=noninteractive verbose apt-get -qq -y install postfix postfix-mysql sasl2-bin libsasl2-modules libsasl2-modules-sql



#   chown mailboxes:mailboxes /srv/mailboxes/default.sieve
#   touch /var/log/dovecot.log
#   chmod +666 /var/log/dovecot.log
#   touch /var/log/dovecot-info.log
#   chmod +666 /var/log/dovecot-info.log
#   touch /var/log/dovecot-debug.log
#   chmod +666 /var/log/dovecot-debug.log

#   echo_magenta "Création de l'utilisateur/groupe spamd"
#   [ $(getent group spamd) ] || verbose groupadd spamd --gid 202
#   [ $(getent passwd spamd) ] || verbose useradd -g spamd -s /bin/false -d /var/log/spamassassin --uid 202 spamd

#   echo_magenta "Création du fichier de log SPAMASSASSIN"
#   verbose mkdir -p /var/log/spamassassin
#   verbose chown spamd:spamd /var/log/spamassassin

#   echo_magenta "Activation de SPAMASSASSIN au lancement de la machine"
#   verbose systemctl -q enable spamassassin


#   sed -i 's/#loadplugin Mail::SpamAssassin::Plugin::AWL/loadplugin Mail::SpamAssassin::Plugin::AWL/g' /etc/mail/spamassassin/v310.pre
#   verbose sa-update



#   verbose chown clamav:clamav /etc/clamav/virusaction.sh
#   verbose chmod 755 /etc/clamav/virusaction.sh

#   echo_magenta "Installation d'OPENDKIM"
#   if [ ! -f /etc/dkim/keys/$DOMAIN/mail.private ]
#   then
#     verbose mkdir -p /etc/dkim/keys/$DOMAIN
#     verbose opendkim-genkey -D /etc/dkim/keys/$DOMAIN -d $DOMAIN -s mail
#     verbose chown opendkim:opendkim -R /etc/dkim
#   fi

#   if [ ! -f /etc/dkim/KeyTable ] || ! grep -q "mail._domainkey.$DOMAIN $DOMAIN:mail:/etc/dkim/keys/$DOMAIN/mail.private" /etc/dkim/KeyTable
#   then
#     echo "mail._domainkey.$DOMAIN $DOMAIN:mail:/etc/dkim/keys/$DOMAIN/mail.private" >> /etc/dkim/KeyTable
#   fi

#   if [ ! -f /etc/dkim/SigningTable ] || ! grep -q "*@$DOMAIN mail._domainkey.$DOMAIN" /etc/dkim/SigningTable
#   then
#     echo "*@$DOMAIN mail._domainkey.$DOMAIN" >> /etc/dkim/SigningTable
#   fi

#   if [ ! -f /etc/dkim/TrustedHosts ] || ! grep -q "$DOMAIN" /etc/dkim/TrustedHosts
#   then
#     echo "$DOMAIN" >> /etc/dkim/TrustedHosts
#   fi
#   verbose sed -i 's/SOCKET=local:$RUNDIR\/opendkim.sock/SOCKET="inet:8891:localhost"/g' /etc/default/opendkim

#   echo_magenta "Installation d'OPENDMARC"

#   sudo mkdir -p /etc/opendmarc/
#   sudo mkdir -p /var/spool/postfix/opendmarc
#   sudo chown opendmarc:opendmarc /var/spool/postfix/opendmarc -R
#   sudo chmod 750 /var/spool/postfix/opendmarc/ -R
#   sudo adduser -q postfix opendmarc &> /dev/null
#   echo "localhost" > /etc/opendmarc/ignore.hosts
#   echo "10.0.0.0/24" >> /etc/opendmarc/ignore.hosts
#   verbose sed -i 's/SOCKET=local:$RUNDIR\/opendmarc.sock/SOCKET="inet:8892@localhost"/g' /etc/default/opendmarc
#   verbose systemctl enable opendmarc -q

#   echo_magenta "Redémarrage des services"
#   verbose systemctl restart clamav-daemon
#   verbose systemctl restart clamav-milter
#   verbose systemctl restart spamassassin
#   verbose systemctl restart spamass-milter
#   verbose systemctl restart postfix
#   verbose systemctl restart dovecot
#   verbose systemctl restart opendkim
#   verbose systemctl restart opendmarc