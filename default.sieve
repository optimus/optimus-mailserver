require ["copy","date","fileinto","relational","vacation"];
# rule:[Antispam]
if allof (header :contains "subject" "***SPAM***")
{
        fileinto "Junk";
}
# rule:[Notifications Postmaster]
if allof (header :contains "to" "postmaster@$DOMAIN")
{
        fileinto "Notifications";
}
